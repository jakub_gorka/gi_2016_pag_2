import re
import redis
import time

time_file_search = 0;
out_file_search = '';

time_start = time.clock()

content = [];
with open('HITOS_Architectural_2006-10-25.ifc') as f:
    content = f.readlines();
time_load = time.clock();
pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0);
db = redis.Redis(connection_pool=pool);
time_db_init = time.clock();
my_regexp = '[=]';    
for line in content:
    if line != '' and line[0] == '#':
        myfind = re.split(my_regexp, line);
        myfind[1] = myfind[1][0:-2];
        db.set(myfind[0], myfind[1]);
time_db_load = time.clock();



for line in content:
    if line != '' and line[0] == '#':
        myfind = re.split(my_regexp, line);
        myfind[1] = myfind[1][0:-2];
        if myfind[0] == '#2046692':
            out_file_search = myfind[1];
            time_file_search = time.clock();
                    
out_db_search = db.get('#2046692');
time_db_search = time.clock();

print('time for loading file: ' + str(time_load - time_start))
print('time for db initialization: ' + str(time_db_init - time_load))
print('time for filling db: ' + str(time_db_load - time_db_init))
print('time for file search: ' + str(time_file_search - time_db_load))
print('time for db search: ' + str(time_db_search - time_file_search))

