with open('HITOS_Architectural_2006-10-25.ifc') as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

line_cnt = 0;
class_cnt = {};

for line in content:
    if line != '' and line[0] == '#':
        line_cnt += 1;
        class_cur_2 = line.split('=',1);
        class_cur = class_cur_2[1].split('(',1);
        if class_cur[0] in class_cnt:
            class_cnt[class_cur[0]] += 1;
        else:
            class_cnt[class_cur[0]] = 1;   
        
print("\nIlosc linii: " + str(line_cnt));
print("\nIlosc obiektow klasy \"IFCDOORSTYLE\": " + str(class_cnt['IFCDOORSTYLE']));
print("\nLista klas: ");
for key in class_cnt.keys():
    print(key);
print("\nLiczba obiektow klas: ");
for key in class_cnt.keys():
    print(str(key) + " - " + str(class_cnt[key]))


