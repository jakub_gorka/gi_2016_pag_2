import re

class Feature:
    def __init__(self, intype, data):
        self.type = intype;
        self.data = data;
        self.parent = '';
        self.children = set();
        
class Relation:
    def __init__(self, parent, children):
        self.parent = parent;
        self.children = children;
    def set_relation(self, features):
        for child in self.children:
            features[self.parent].children.add(child);
            features[child].parent = self.parent;
            
def print_up(features, ident, indentation):
    if features[ident].parent != '':
        indentation = print_up(features, features[ident].parent, indentation);
    outstr = '';
    for x in range(0,indentation):
        outstr = outstr + '  '
    outstr = outstr + str(features[ident].data[2]);
    print(outstr);

    return indentation + 1;

def print_down(features, ident, indentation):
    outstr = '';
    for x in range(0,indentation):
        outstr = outstr + '  '
    outstr = outstr + str(features[ident].data[2]);
    print(outstr);
    if len(features[ident].children) != 0:
        for child in features[ident].children:
            print_down(features, child, indentation + 1);
    return

def print_features(features,ident):
    indentation = 0;
    indent = print_up(features,ident,indentation);
    print_down(features,ident,indent);
    return

with open('HITOS_Architectural_2006-10-25.ifc') as f:
    content = f.readlines()

lf_objects = {'IFCPROJECT', 'IFCSITE','IFCBUILDING','IFCBUILDINGSTOREY','IFCSPACE'};
lf_rels = {'IFCRELAGGREGATES'};
my_regexp = '[=()]';
features = {};
relations = {};

for line in content:
    if line != '' and line[0] == '#':
        myfind = re.split(my_regexp, line);
        if myfind[1] in lf_objects:
            myfind_data = re.split('[,]', myfind[2].replace('\'',''))
            features[myfind[0]] = Feature(myfind[1],myfind_data);
        elif myfind[1] in lf_rels:
            myfind_data = re.split('[,]', myfind[2].replace('\'',''))
            myfind_rels = re.split('[,]', myfind[3].replace('\'',''))
            relations[myfind[0]] = Relation(myfind_data[4],myfind_rels);

# nie jestem pewny czy istnieja juz obiekty
for rel in relations:
    relations[rel].set_relation(features);
    
print_features(features, '#42');



