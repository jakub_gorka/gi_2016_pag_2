import re
import redis
import time
import pymongo
import json

def mysplit(line):
    outset = []
    flag = 0
    prev = 0
    now = -1
    for char in line:
        now = now + 1;
        if char == '(':
            flag = flag + 1;
        elif char == ')':
            flag = flag - 1;
        elif char == ',' and flag == 0:
            outset.append(line[prev:now])
            prev = now+1
    outset.append(line[prev:now])
    return outset

def is_number_regex(s):
    """ Returns True is string is a number. """
    if re.match("^-?\d+?\.\d+?$", s) is None:
        return s.isdigit()
    return True

def myparsedot(word):
    s = word[0] == '.'
    f = word[-1] == '.'
    #print (word)
    if not s and not f:
        if is_number_regex(word):
            return float(word)
        else:
            return word
    elif s and f:
        return word[1:-1]
    elif word[0] == '-':
        return float(word + '0')
    else:
        return float('0' + word + '0')

    
        


    
def myparsedotbulk(tab):
    out = []
    for a in tab:
        out.append(myparsedot(a));
    return out;

structures = {'IFCPROJECT':['GlobalId','OwnerHistory','Name','Description','ObjectType','LongName','Phase','RepresentationContexts','UnitsInContext'],
              'IFCCARTESIANPOINT':['Coordinates'],
              'IFCBUILDING':['GlobalId','OwnerHistory','Name','Description','ObjectType','ObjectPlacement','Representation','LongName','CompositionType','ElevationOfRefHeight','ElevationOfTerrain','BuildingAddress'],
              'IFCPROPERTYSINGLEVALUE':['Name','Description','NominalValue','Unit'],
              'IFCCOMPLEXPROPERTY':['Name','Description','UsageName','HasProperties'],
              'IFCPOLYLOOP':['Polygon'],
              'IFCFACEOUTERBOUND':['Bound','Orientation']
               }


time_start = time.clock()

content = [];
with open('HITOS_Architectural_2006-10-25.ifc') as f:
    content = f.readlines();
time_load = time.clock();

connection = pymongo.MongoClient("mongodb://localhost");
db = connection.ifc;
hitos = db.hitos;

time_db_init = time.clock();

my_regexp = '[=]';
my_regexp2 = '[(]';
my_regexp3 = '[,]';
cnt = 0
for line in content:
    if line != '' and line[0] == '#':
        myfind = re.split(my_regexp, line);
        myfind[1] = myfind[1][0:-2];
        myfind2 = re.split(my_regexp2, myfind[1], 1);
        myfind3 = mysplit(myfind2[1]);
        
        if myfind2[0] in structures:
            cnt = cnt+1
            if cnt % 10000 == 0:
                print(cnt)
            mylist = list(zip(structures[myfind2[0]], myfind3))
            mylist.append(("IfcClass",myfind2[0]))
            mylist.append(("IfcId",myfind[0]))
            dictionary = {}
            for d in mylist:
                if d[1][0] != '(':
                    dictionary[d[0]] = myparsedot(d[1]);
                else:
                    dictionary[d[0]] = myparsedotbulk(mysplit(d[1][1:-1]));
            hitos.insert_one(dictionary) #time for filling db: 467.957684488 with ~630000 entities, slow
time_db_load = time.clock();
print(hitos.find_one({'IfcId':'#62'})) #time for db search: 0.0845846140665
time_db_search = time.clock();
# 42-project 116-cartesianPoint 62 building
print('\n\n\ntime for loading file: ' + str(time_load - time_start))
print('time for db initialization: ' + str(time_db_init - time_load))
print('time for filling db: ' + str(time_db_load - time_db_init))
print('time for db search: ' + str(time_db_search - time_db_load))

