import re
import redis
import time

def mysplit(line):
    outset = []
    flag = 0
    prev = 0
    now = -1
    for char in line:
        now = now + 1;
        if char == '(':
            flag = flag + 1;
        elif char == ')':
            flag = flag - 1;
        elif char == ',' and flag == 0:
            outset.append(line[prev:now])
            prev = now+1
    return outset

structures = {'IFCPROJECT':['GlobalId','OwnerHistory','Name','Description','ObjectType','LongName','Phase','RepresentationContexts','UnitsInContext'],
              'IFCCARTESIANPOINT':['Coordinates'],
              'IFCBUILDING':['GlobalId','OwnerHistory','Name','Description','ObjectType','ObjectPlacement','Representation','LongName','CompositionType','ElevationOfRefHeight','ElevationOfTerrain','BuildingAddress'],
              'IFCPROPERTYSINGLEVALUE':['Name','Description','NominalValue','Unit'],
              'IFCCOMPLEXPROPERTY':['Name','Description','UsageName','HasProperties'],
              'IFCPOLYLOOP':['Polygon'],
              'IFCFACEOUTERBOUND':['Bound','Orientation']
               }

time_file_search = 0;
out_file_search = '';

time_start = time.clock()

content = [];
with open('HITOS_Architectural_2006-10-25.ifc') as f:
    content = f.readlines();
time_load = time.clock();

pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0);
db = redis.StrictRedis(connection_pool=pool);
time_db_init = time.clock();

my_regexp = '[=]';
my_regexp2 = '[(]';
my_regexp3 = '[,]';
for line in content:
    if line != '' and line[0] == '#':
        myfind = re.split(my_regexp, line);
        myfind[1] = myfind[1][0:-2];
        myfind2 = re.split(my_regexp2, myfind[1], 1);
        myfind3 = mysplit(myfind2[1]);
        if myfind2[0] in structures:
            dictionary = list(zip(structures[myfind2[0]], myfind3))
            dictionary.append(("IfcClass",myfind2[0]))
            for d in dictionary:
                db.hset("#42", d[0], d[1]) # time for filling db: 83.5660081824
time_db_load = time.clock();

print(db.hget("#42", "IfcClass"))
print(db.hgetall('#42'));
time_db_search = time.clock();

print('\n\n\ntime for loading file: ' + str(time_load - time_start))
print('time for db initialization: ' + str(time_db_init - time_load))
print('time for filling db: ' + str(time_db_load - time_db_init))
print('time for db search: ' + str(time_db_search - time_db_load))

